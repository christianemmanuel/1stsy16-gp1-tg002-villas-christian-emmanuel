﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour
{
	public float speed;
    private GameObject Player;
    private GameObject Enemy;
    public Transform prefab;
    private float target;


    // Use this for initialization
    void Start()
    {
        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        InvokeRepeating("Boom", 0.2f, 0.7f);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void Boom()
    {
        Transform clone;
        clone = Instantiate(prefab, transform.position, Enemy.transform.rotation) as Transform;

    }
}