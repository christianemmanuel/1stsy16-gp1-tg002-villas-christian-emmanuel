﻿using UnityEngine;
using System.Collections;

public class CannonBall : MonoBehaviour
{

    public float speed;
    private GameObject Enemy;
    private Vector3 target;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        target = Enemy.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);


        if (transform.position == target)
            Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);

        }

    }

}