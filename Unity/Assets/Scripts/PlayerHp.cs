﻿using UnityEngine;
using System.Collections;

public class PlayerHp : MonoBehaviour 
{
	
	public float NumberOfHitsToDestroy;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "EnemyCannonBall")
		{
			NumberOfHitsToDestroy--;
			
			if(NumberOfHitsToDestroy <= 0.0f)
				Destroy(gameObject);
			
		}
		
	}
	
}