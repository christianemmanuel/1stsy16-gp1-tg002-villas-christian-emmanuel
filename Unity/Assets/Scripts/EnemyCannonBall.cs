﻿using UnityEngine;
using System.Collections;

public class EnemyCannonBall : MonoBehaviour
{

	public float speed;
	private GameObject Player;
	private Vector3 target;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		Player = GameObject.FindGameObjectWithTag("Player");
		target = Player.transform.position;
		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
		
		
		if (transform.position == target)
			Destroy(gameObject); 
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player")
		{
			Destroy(gameObject);
			
		}
		
	}
	
}