﻿using UnityEngine;
using System.Collections;

public class EnemyHP : MonoBehaviour 
{

	public float NumberOfHitsToDestroy;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "CannonBall") 
		{
			NumberOfHitsToDestroy = 0f;
			
			if(NumberOfHitsToDestroy <= 0f)
				Destroy(gameObject);
			
		}
		
	}
	
}