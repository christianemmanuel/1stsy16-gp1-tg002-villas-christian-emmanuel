﻿using UnityEngine;
using System.Collections;

public class EnemyMoveScript : MonoBehaviour 
{

	private GameObject Player;
	public float speed;
	private Vector3 target;
	
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Player = GameObject.FindGameObjectWithTag ("Player");
		
		target = Player.transform.position;
		
		transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);
		
		
		
		
	}
}