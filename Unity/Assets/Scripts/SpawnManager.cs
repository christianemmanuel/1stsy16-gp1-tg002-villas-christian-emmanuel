﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour 
{

	public GameObject player;


	// Use this for initialization
	void Start () 
	{
		StartCoroutine (EnemySpawn ());
	
	}

	IEnumerator EnemySpawn()
	{
		while(true)
		{
			Instantiate(player, transform.position, Quaternion.identity);
			yield return new WaitForSeconds(10f);
		}
	}

}
